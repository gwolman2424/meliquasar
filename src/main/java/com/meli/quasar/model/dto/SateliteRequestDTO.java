package com.meli.quasar.model.dto;

import java.util.List;

public class SateliteRequestDTO {

    private List<DataSateliteDTO> satellites;

    public List<DataSateliteDTO> getSatellites() {
        return satellites;
    }

    public void setSatellites(List<DataSateliteDTO> satellites) {
        this.satellites = satellites;
    }
}
