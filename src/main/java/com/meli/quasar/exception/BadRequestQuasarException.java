package com.meli.quasar.exception;

import org.springframework.http.HttpStatus;

public class BadRequestQuasarException extends QuasarException {
    public BadRequestQuasarException(String message) {
        super(message);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }

    public BadRequestQuasarException(String message, Throwable cause) {
        super(message, cause);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }
}
