package com.meli.quasar.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lemmingapex.trilateration.LinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;
import com.meli.quasar.exception.BadRequestQuasarException;
import com.meli.quasar.exception.NotFoundQuasarException;
import com.meli.quasar.exception.QuasarException;
import com.meli.quasar.model.Satelite;
import com.meli.quasar.model.dto.*;
import com.meli.quasar.repository.SateliteRepository;
import com.meli.quasar.service.QuasarService;
import com.meli.quasar.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class QuasarServiceImpl implements QuasarService {
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private SateliteRepository sateliteRepository;
    private static final Logger logger = LoggerFactory.getLogger(QuasarServiceImpl.class);

    /**
     * Save data receive to satelite db
     *
     * @param name
     * @param sateliteSplitRequestDTO
     * @return
     * @throws JsonProcessingException
     * @throws QuasarException
     */
    @Override
    public SateliteSplitDTO saveDataSatelite(String name, SateliteSplitDTO sateliteSplitRequestDTO) throws JsonProcessingException, QuasarException {
        Satelite satelite = this.sateliteRepository.findOneByName(name);
        if (Objects.isNull(satelite)) {
            throw new NotFoundQuasarException(String.format("Satelite %s not found", name));
        }
        this.validate(sateliteSplitRequestDTO);
        satelite.setDistance(sateliteSplitRequestDTO.getDistance());
        satelite.setMessage(objectMapper.writeValueAsString(sateliteSplitRequestDTO.getMessage()));
        this.sateliteRepository.save(satelite);
        return sateliteSplitRequestDTO;
    }

    /**
     * If data is complete in db satelite, returns location y message
     *
     * @return
     * @throws BadRequestQuasarException
     */
    public SateliteResponseDTO getDataSatelite() throws BadRequestQuasarException {
        List<Satelite> satellites = this.sateliteRepository.findAll();
        List<DataSateliteDTO> dataSatellitesDTO = satellites.stream().map(this::parseSateliteToDTO).collect(Collectors.toList());
        return this.getPositionAndMessage(dataSatellitesDTO);
    }

    /**
     * Returns position and message
     *
     * @param dataSatellites data
     * @return
     * @throws BadRequestQuasarException
     */
    @Override
    public SateliteResponseDTO getPositionAndMessage(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException {
        this.validate(dataSatellites);
        SateliteResponseDTO sateliteResponseDTO = new SateliteResponseDTO();
        sateliteResponseDTO.setMessage(this.getMessage(dataSatellites));
        sateliteResponseDTO.setPosition(this.getLocation(dataSatellites));
        return sateliteResponseDTO;
    }

    /**
     * Returns position
     *
     * @param dataSatellites data
     * @return
     * @throws BadRequestQuasarException
     */
    @Override
    public PositionDTO getLocation(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException {
        double[] distances = dataSatellites.stream().mapToDouble(x -> x.getDistance().doubleValue()).toArray();
        LinearLeastSquaresSolver linearLeastSquaresSolver = new LinearLeastSquaresSolver(new TrilaterationFunction(Constant.SATELLITES_POSITIONS, distances));
        double[] xy = linearLeastSquaresSolver.solve().toArray();
        PositionDTO positionDTO = new PositionDTO();
        if (Objects.nonNull(xy) && xy.length == 2) {
            positionDTO.setX(xy[0]);
            positionDTO.setY(xy[1]);
            return positionDTO;
        }
        throw new BadRequestQuasarException("Error to calculate location.");
    }

    /**
     * Returns message
     *
     * @param dataSatellites data
     * @return
     * @throws BadRequestQuasarException
     */
    @Override
    public String getMessage(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException {
        //  calculate size without defasaje
        Integer maxSize = dataSatellites.stream().map(x -> x.getMessage().size()).mapToInt(z -> z).max().getAsInt();
        if (maxSize <= 0) {
            throw new BadRequestQuasarException("Message size invalid.");
        }
        AtomicInteger atomicMaxSize = new AtomicInteger(maxSize);
        dataSatellites.stream().map(x -> x.getMessage()).forEach(x -> {
            if (x.size() < atomicMaxSize.get()) {
                Integer stop = atomicMaxSize.get() - x.size();
                for (Integer i = 0; i < stop; i++) {
                    x.add(i, "");
                }
            }
        });
        String[] listMessage = new String[maxSize];
        // merge arrays, reverse mode
        for (int i = (maxSize - 1); i >= 0; i--) {
            AtomicInteger j = new AtomicInteger(i);
            dataSatellites.stream().map(x -> x.getMessage()).forEach(x -> {
                if (Objects.nonNull(x.get(j.get())) && !x.get(j.get()).trim().isEmpty()) {
                    listMessage[j.get()] = (x.get(j.get()));
                }
            });
        }
        return Arrays.stream(listMessage).filter(x -> Objects.nonNull(x) && !x.trim().isEmpty()).collect(Collectors.joining(" "));
    }

    /**
     * Validate list
     *
     * @param dataSatellites
     * @throws BadRequestQuasarException
     */
    public void validate(List<DataSateliteDTO> dataSatellites) throws BadRequestQuasarException {
        for (DataSateliteDTO dataSateliteDTO : dataSatellites) {
            try {
                if (Objects.isNull(dataSatellites) || dataSatellites.isEmpty()) {
                    throw new BadRequestQuasarException(String.format("Number of satellites not valid, enter more or equal than three"));
                }
                this.validate(dataSateliteDTO);
            } catch (BadRequestQuasarException e) {
                logger.error("Error information incomplete", e);
                throw new BadRequestQuasarException(String.format("Incomplete information to calculate, satelite %s, %s", dataSateliteDTO.getName(), e.getMessage()));
            }
        }
    }

    /**
     * Method to validate
     *
     * @param dataSatelite
     */
    private void validate(ISatelite dataSatelite) throws BadRequestQuasarException {
        if (Objects.isNull(dataSatelite.getDistance())) {
            throw new BadRequestQuasarException("Distance can not be null");
        }
        if (Objects.isNull(dataSatelite.getMessage()) || dataSatelite.getMessage().size() <= 0) {
            throw new BadRequestQuasarException("Message can not be null or empty");
        }
    }

    /**
     * Parse satelite to DataSateliteDTO
     *
     * @param satelite data salitelite
     * @return
     * @throws BadRequestQuasarException
     */
    private DataSateliteDTO parseSateliteToDTO(Satelite satelite) {
        List<String> message = null;
        if (Objects.nonNull(satelite.getMessage())) {
            try {
                message = objectMapper.readValue(satelite.getMessage(), new TypeReference<List<String>>() {
                });
            } catch (JsonProcessingException e) {
            }
        }
        return new DataSateliteDTO(satelite.getName(), satelite.getDistance(), message);
    }
}
