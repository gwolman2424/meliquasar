package com.meli.quasar.controller;

import com.meli.quasar.exception.BadRequestQuasarException;
import com.meli.quasar.model.dto.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

	private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(BadRequestQuasarException.class)
	public ResponseEntity<ResponseDTO> fileNotFoundExceptionHandler(BadRequestQuasarException ex) {
		logger.error("BadRequestQuasarException", ex);
		ResponseDTO error = new ResponseDTO(ex.getHttpStatus().value(), ex.getMessage());
		return new ResponseEntity<>(error, ex.getHttpStatus());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseDTO> fileNotFoundExceptionHandler(Exception ex) {
		logger.error("Exception", ex);
		ResponseDTO error = new ResponseDTO(500, ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
